﻿using UnityEngine;
using System.Collections;

public class SlinkyController : MonoBehaviour {
    public int direction;
    public float counter;

	void Update () {
        if (this.counter >= 3)
        {
            switchDirections();
        } else
        {
            this.counter += Time.deltaTime;
        }

        switch (this.direction)
        {
            case -1:
                GetComponent<Move>().MoveLeft();
                break;
            case 1:
                GetComponent<Move>().MoveRight();
                break;
            default:
                GetComponent<Move>().speed = 0f;
                break;
        }	
	}

    void OnCollisionEnter(Collision c)
    {
        switchDirections();

        if (c.gameObject.tag == "Player")
        {
            if (c.gameObject.GetComponent<ComboSystem>().state != "")
            {
                if (c.gameObject.GetComponent<ComboSystem>().state == "punch")
                {
                    GetComponent<Health>().health -= 1;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "kick")
                {
                    GetComponent<Health>().health -= 2;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "uppercut")
                {
                    GetComponent<Health>().health -= 2;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "piledrive")
                {
                    GetComponent<Health>().health -= 2;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "spinkick")
                {
                    GetComponent<Health>().health -= 3;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "crouchkick")
                {
                    GetComponent<Health>().health -= 1;
                }

                GetComponent<Rigidbody>().velocity += c.gameObject.transform.right * c.gameObject.GetComponent<PlayerController>().direction * 5;
            }
        }
    }

    void switchDirections()
    {
        this.counter = 0f;
        if (direction > 0)
        {
            this.direction = -1;
        }
        else if (direction < 0)
        {
            this.direction = 1;
        }
    }
}
