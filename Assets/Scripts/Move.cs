﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
    public float speed;
	
    public void MoveLeft()
    {
        GetComponent<Rigidbody>().MovePosition(new Vector3(transform.position.x - speed, transform.position.y, transform.position.z));
    }

    public void MoveRight()
    {
        GetComponent<Rigidbody>().MovePosition(new Vector3(transform.position.x + speed, transform.position.y, transform.position.z));
    }

    public void Jump() {
        GetComponent<Rigidbody>().velocity = transform.up * (speed * 50);
    } 

    public void MoveToPosition(Vector3 pos)
    {
        Vector3 direction = (transform.position - pos).normalized;
        GetComponent<Rigidbody>().MovePosition(transform.position - direction * speed * Time.deltaTime);
    }
}
