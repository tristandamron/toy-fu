﻿using UnityEngine;
using System.Collections;

public class FighterController : MonoBehaviour {
    public GameObject target;
    public string[] moves = {"punch", "kick"};

    float counter;
    bool falling;

    void OnTriggerEnter (Collider c)
    {
        if (c.tag == "Player")
        {
            this.target = c.gameObject;
        }
    }

    void OnCollisionEnter(Collision c)
    {
        this.falling = false;

        if (c.gameObject.tag == "Player" && c.gameObject.GetComponent<ComboSystem>().state != "")
        {
            if (c.gameObject.GetComponent<ComboSystem>().state == "punch")
            {
                GetComponent<Health>().health -= 1;
            }
            else if (c.gameObject.GetComponent<ComboSystem>().state == "kick")
            {
                GetComponent<Health>().health -= 2;
            }
            else if (c.gameObject.GetComponent<ComboSystem>().state == "uppercut")
            {
                GetComponent<Health>().health -= 2;
            }
            else if (c.gameObject.GetComponent<ComboSystem>().state == "piledrive")
            {
                GetComponent<Health>().health -= 2;
            }
            else if (c.gameObject.GetComponent<ComboSystem>().state == "spinkick")
            {
                GetComponent<Health>().health -= 3;
            }
            else if (c.gameObject.GetComponent<ComboSystem>().state == "crouchkick")
            {
                GetComponent<Health>().health -= 1;
            }

            GetComponent<Rigidbody>().velocity += c.gameObject.transform.right * c.gameObject.GetComponent<PlayerController>().direction * 10;
        }
    }

    void Update () {
        GetComponent<Move>().MoveToPosition(target.transform.position);
        this.counter += Time.deltaTime;

        if (counter >= 0.5f)
        {
            GetComponent<ComboSystem>().keys.Add(this.moves[Random.Range(0, 1)]);
            this.counter = 0f;

            if (this.target != null && this.target.GetComponent<ComboSystem>().state == "jump" && !this.falling)
            {
                falling = true;
                GetComponent<Move>().speed = GetComponent<Move>().speed / 40;
                GetComponent<Move>().Jump();
                GetComponent<Move>().speed = 8f;
            }
        }
	}
}
