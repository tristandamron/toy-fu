﻿using UnityEngine;
using System.Collections;

public class MonkeyController : MonoBehaviour {

	void OnTriggerStay (Collider c)
    {
        if (c.tag == "Player")
        {
            GetComponent<Move>().MoveToPosition(c.transform.position);
        }
    }

    void OnCollisionEnter (Collision c)
    {
        GetComponent<Health>().health = 0f;
    }
}
