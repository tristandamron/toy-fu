﻿using UnityEngine;
using System.Collections;

public class DarumaDollController : MonoBehaviour {

    public float counter;
    bool falling;
	
	void Update () {
        this.counter += Time.deltaTime;

        if (this.counter >= 2f && !this.falling)
        {
            GetComponent<Move>().Jump();
            this.falling = true;
            this.counter = 0f;
        } 
	}

    void OnCollisionEnter (Collision c)
    {
        this.falling = false;

        if (c.gameObject.tag == "Player")
        {
            if (c.gameObject.GetComponent<ComboSystem>().state != "")
            {
                GetComponent<Rigidbody>().velocity += c.gameObject.transform.right * c.gameObject.GetComponent<PlayerController>().direction * 10;
                if (c.gameObject.GetComponent<ComboSystem>().state == "punch")
                {
                    GetComponent<Health>().health -= 1;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "kick")
                {
                    GetComponent<Health>().health -= 2;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "uppercut")
                {
                    GetComponent<Health>().health -= 2;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "piledrive")
                {
                    GetComponent<Health>().health -= 2;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "spinkick")
                {
                    GetComponent<Health>().health -= 3;
                }
                else if (c.gameObject.GetComponent<ComboSystem>().state == "crouchkick")
                {
                    GetComponent<Health>().health -= 1;
                }
            }
        }
    }
}
