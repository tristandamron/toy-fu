﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour {
    public int direction;
    public bool paused;
    public GameObject pauseMenu;
    bool falling = false;
    float pauseCounter;

	void Update () {
        this.pauseCounter += Time.deltaTime;

	    if (Input.GetAxisRaw("Horizontal") > 0f)
        {
            this.direction = 1;
            GetComponent<Move>().MoveRight();
        } else if (Input.GetAxisRaw("Horizontal") < 0f)
        {
            this.direction = -1;
            GetComponent<Move>().MoveLeft();
        }

        if (Input.GetAxisRaw("Jump") > 0f && !falling)
        {
            GetComponent<Move>().Jump();
            falling = true;
        }

        if (Input.GetAxisRaw("Pause") > 0f && this.pauseCounter >= 0.5f)
        {
            this.pauseCounter = 0f;
            this.paused = !this.paused;
        }

        if (this.paused)
        {
            GetComponent<Move>().speed = 0f;
        }
        else
        {
            GetComponent<Move>().speed = 0.05f;
        }

        this.pauseMenu.SetActive(this.paused);
    }

    void OnTriggerStay(Collider c)
    {
        if (c.tag == "Water")
        {
            falling = false;
        }
    }

    void OnCollisionEnter(Collision c)
    {
        falling = false;

        if (c.gameObject.tag == "Slinky" && GetComponent<ComboSystem>().state == "")
        {
            GetComponent<Health>().health -= 1;
            switchDirections();
            GetComponent<Rigidbody>().velocity += transform.right * this.direction * 4;
        } else if (c.gameObject.tag == "DarumaDoll" && GetComponent<ComboSystem>().state == "")
        {
            GetComponent<Health>().health -= 1;
            switchDirections();
            GetComponent<Rigidbody>().velocity += transform.right * this.direction * 7;
        } else if (c.gameObject.tag == "Fighter")
        {
            if (c.gameObject.GetComponent<ComboSystem>().state == "punch")
            {
                GetComponent<Health>().health -= 1;
                switchDirections();
                GetComponent<Rigidbody>().velocity += transform.right * this.direction * 8;
            } else if (c.gameObject.GetComponent<ComboSystem>().state == "kick")
            {
                GetComponent<Health>().health -= 2;
                GetComponent<Rigidbody>().velocity += transform.right * this.direction * 8;
            }
        }
    }

    void switchDirections()
    {
        if (this.direction > 0)
        {
            this.direction = -1;
        }
        else if (this.direction < 0)
        {
            this.direction = 1;
        }
    }
}
