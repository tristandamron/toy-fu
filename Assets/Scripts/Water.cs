﻿using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour {
    public float speed;
    public GameObject player;
    public bool freezePlayer;
    float playerSpeed;

    void OnTriggerEnter(Collider c)
    {
        if (c.tag == "Player")
        {
            this.player = c.gameObject;

            if (this.freezePlayer)
            {
                this.playerSpeed = this.player.GetComponent<Move>().speed;
                this.player.GetComponent<Move>().speed = 0f;
            }
        }
    }

    void OnTriggerStay(Collider c)
    {
        if (c.tag == "Player")
        {
            this.player = c.gameObject;

            if (this.player.GetComponent<ComboSystem>().state != "jump")
            {
                this.player.GetComponent<Rigidbody>().velocity = transform.right * speed;
            }
        }
    }

    void OnTriggerExit(Collider c)
    {
        if (this.freezePlayer)
        {
            this.player.GetComponent<Move>().speed = this.playerSpeed;
        }
    }
}
