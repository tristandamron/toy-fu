﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float distance;
    public float delay;

    void Update()
    {
        Vector3 pos = new Vector3(target.position.x, target.position.y + distance, target.position.z - 4.25f);
        transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime / delay);
    }
}

