﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
    public float health;
    public static float maxHealth;

    void Update()
    {
        if (health <= 0f)
        {
            Destroy(gameObject);
        }
    }
}
