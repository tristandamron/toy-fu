﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class ComboSystem : MonoBehaviour {
    public List<string> keys;
    public string state;
    float counter;
    float coolDown = 2f;

    void Start () {
        this.keys = new List<string>();
	}
	
	void Update () {
        this.counter += Time.deltaTime;

        if (this.counter >= 1f)
        {
            this.keys.Clear();
            this.counter = 0f;
            this.state = "";
        }

        if (this.coolDown < 0.5f)
        {
            this.coolDown += Time.deltaTime;
            this.keys.Clear();
        } else
        {
            string combo = "";
            foreach (string key in keys)
            {
                combo += key;
                if (Regex.IsMatch(combo, "uppunch"))
                {
                    state = "uppercut";
                    this.keys.Clear();
                    GetComponent<Rigidbody>().velocity += transform.up * 2;
                    this.coolDown = 0f;
                } else if (Regex.IsMatch(combo, "movingpunch"))
                {
                    state = "piledrive";
                    GetComponent<Rigidbody>().velocity += transform.right * (Input.GetAxis("Horizontal") * 2);
                    this.keys.Clear();
                    this.coolDown = 0f;
                } else if (Regex.IsMatch(combo, "punch"))
                {
                    state = "punch";
                    this.keys.Clear();
                    this.coolDown = 0f;
                } else if (Regex.IsMatch(combo, "movingkick"))
                {
                    state = "spinkick";
                    GetComponent<Rigidbody>().velocity += transform.right * (Input.GetAxis("Horizontal") * 2);
                    this.keys.Clear();
                    this.coolDown = 0f;
                } else if (Regex.IsMatch(combo, "downkick"))
                {
                    state = "crouchkick";
                    this.keys.Clear();
                    this.coolDown = 0f;
                } else if (Regex.IsMatch(combo, "kick"))
                {
                    state = "kick";
                    this.keys.Clear();
                    this.coolDown = 0f;
                } else if (Regex.IsMatch(combo, "jump"))
                {
                    state = "jump";
                    this.keys.Clear();
                }
            }
        }    
	}
}
