﻿using UnityEngine;
using System.Collections;

public class ComboTest : MonoBehaviour {
    public ComboSystem combosys;

	void Start ()
    {
        this.combosys = GetComponent<ComboSystem>();
    }

	void Update () {
        if (Input.GetAxisRaw("Punch") > 0f)
        {
            this.combosys.keys.Add("punch");
        }

        if (Input.GetAxisRaw("Vertical") > 0f)
        {
            this.combosys.keys.Add("up");
        } 

        if (Input.GetAxisRaw("Vertical") < 0f)
        {
            this.combosys.keys.Add("down");
        }

        if (Input.GetAxisRaw("Kick") > 0f)
        {
            this.combosys.keys.Add("kick");
        } 

        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            this.combosys.keys.Add("moving");
        }

        if (Input.GetAxisRaw("Jump") > 0f)
        {
            this.combosys.keys.Add("jump");
        }
	}
}
